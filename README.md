# Project: A Agri Calculator.
Hi there!
This is a calculator that I have decided to make in order to help out some friends with how much of x substance or material they should put in their soil for a set size.
Relatively UI-less the moment. Parameters are set in the code itself for the moment, thus the project has to be *unfortunately* rebuilt everytime a new rendition is made with different vars.
I might make a UI for it in the future if I desire, but this was mostly a side-project. If nothing else, probably a way to interact with specific parameters through the terminal.

If you want me to expand on this concept, do let me know.
~ Pikafear

## License
MIT License



