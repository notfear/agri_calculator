// This will be a basic calculator that basically calculates the npk conversions for certain fertilisers and the like.
// It is meant primarily for personal use. Please use at your own discretion!

// Based on:
// -> Rapitest Soil Test Kit values. TODO: Might base it on a more reliable format in the future
// -> The Mini Farming Bible: Brett L. Markham, p87


mod calculators;


fn main() {
    let unit:calculators::AreaConversions = calculators::AreaConversions::new();

    // Change these variables depending on fertilizer / ph requirements and plot size!
    // Might do some UI or CL shtuff for inputs on this.
    let n_fert_perc:f32 = 3.0;
    let n_status:calculators::NPKStatus = calculators::NPKStatus::Depleted;
    let p_fert_perc:f32 = 6.0;
    let p_status:calculators::NPKStatus = calculators::NPKStatus::Depleted;
    let k_fert_perc:f32 = 3.0;
    let k_status:calculators::NPKStatus = calculators::NPKStatus::Depleted;
    let npk_no_feet_sq:f32 = 100.0; // aka: could be 100 sq feet for npk fert

    let fert_ph_change:f32 = 0.5;
    let ph_mat_weight_pounds_feet_sq:f32 = 2.5; // How many pounds per x feet shown below.
    let ph_no_feet_sq:f32 = 100.0; // This one is for ph fert

    let plot_cur_ph:f32 = 4.0; // What ph we cur have.
    let plot_des_ph:f32 = 5.0; // What ph we want.

    let plot_size_meters_sq:f32 = 9.29;


    // Now calculating amount of fertiliser needed for all cases.
    // npk
    let mut npk_calc:calculators::FertNPKCalculator = calculators::FertNPKCalculator::new();

    npk_calc.set_n_params(n_fert_perc, n_status);
    npk_calc.set_p_params(p_fert_perc, p_status);
    npk_calc.set_k_params(k_fert_perc, k_status);
    npk_calc.set_plot_size_meters_squared(plot_size_meters_sq);

    let n_fert_kg_needed:f32 = npk_calc.calculate_n_fert_kg(unit, npk_no_feet_sq);
    let p_fert_kg_needed:f32 = npk_calc.calculate_p_fert_kg(unit, npk_no_feet_sq);
    let k_fert_kg_needed:f32 = npk_calc.calculate_k_fert_kg(unit, npk_no_feet_sq);
    println!("///////////////NPK/////////////////");
    println!("n fert needed: {}", n_fert_kg_needed);
    println!("p fert needed: {}", p_fert_kg_needed);
    println!("k fert needed: {}", k_fert_kg_needed);
    println!("//////////////PH///////////////////");
    //ph
    let mut ph_calc:calculators::PHCalculator = calculators::PHCalculator::new();

    ph_calc.set_ph_change(fert_ph_change);
    ph_calc.set_mat_wei_pou_p_feet(ph_mat_weight_pounds_feet_sq);
    ph_calc.set_cur_ph(plot_cur_ph);
    ph_calc.set_desired_ph(plot_des_ph);
    ph_calc.set_plot_size_meters_squared(plot_size_meters_sq);

    let ph_fert_kg_needed:f32 = ph_calc.calculate_ph_fert_kg(unit, ph_no_feet_sq);

    println!("ph fert needed: {}", ph_fert_kg_needed);


}
