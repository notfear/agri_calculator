// General purpose area converter.
// Also offers imperial -> metric conversion.

mod unit_conversions;

#[derive(Copy, Clone)]
pub struct AreaConversions {
    oz:f32,
    pounds:f32,
    feet_squared:f32,

}

impl AreaConversions {
    pub fn new() -> AreaConversions {
        return AreaConversions {
            oz: 1.0,
            pounds: 1.0,
            feet_squared: 100.0,
        };
    }

    // GENERAL IMPERIAL -> METRIC CONVERSIONS
    fn oz_to_kg(&mut self) -> f32 {
        let kg:f32 = self.oz*unit_conversions::MassUnits::Oz.to_kg();
        return kg;
    }

    fn pounds_to_kg(&mut self) -> f32 {
        let kg:f32 = self.pounds*unit_conversions::MassUnits::Pound.to_kg();
        return kg;
    }

    fn feet_to_meters_squared(&mut self) -> f32 {
        let meter_squared:f32 = self.feet_squared*unit_conversions::DistUnits::Feet.to_meter()*unit_conversions::DistUnits::Feet.to_meter();
        return meter_squared;
    }

    // GENERAL SETTERS.
    pub fn set_oz(&mut self, new_oz:f32) {
        self.oz = new_oz;
    }

    pub fn set_feet(&mut self, new_feet_squared:f32) {
        self.feet_squared = new_feet_squared;
    }

    pub fn set_pounds(&mut self, val:f32) {
        self.pounds = val;
    }

    // AREA CONVERSION
    pub fn get_kg_per_meter_square_using_oz(&mut self) -> f32 {

        let kg_meter_square:f32 = self.oz_to_kg() / self.feet_to_meters_squared();

        return kg_meter_square;

    }

    pub fn get_kg_per_meter_square_using_pounds(&mut self) -> f32 {

        let kg_meter_square:f32 = self.pounds_to_kg() / self.feet_to_meters_squared();

        return kg_meter_square;

    }

}

// This is concerned with the npk calculations that can get us how much of x to put into soil.
// NPK Status uses rapi-tests measurement
pub enum NPKStatus {
    Depleted,
    Deficient,
    Adequate,
}

impl NPKStatus {
    fn return_mod(&mut self) -> f32 {
        match self {
            NPKStatus::Depleted  => return 1.00,
            NPKStatus::Deficient => return 0.50,
            NPKStatus::Adequate  => return 0.25,
        }
    }
}

pub struct FertNPKCalculator {
    n_perc:f32, // Nitrogen
    p_perc:f32, // Phosphorous
    k_perc:f32, // Potassium

    n_status:NPKStatus,
    p_status:NPKStatus,
    k_status:NPKStatus,

    plot_size_meters_squared:f32, // We will need to convert from whatever unit measurement we use to meters!
}

impl FertNPKCalculator {
    pub fn new() -> FertNPKCalculator {
        return FertNPKCalculator {
            n_perc: 0.0,
            p_perc: 0.0,
            k_perc: 0.0,
            n_status: NPKStatus::Depleted,
            p_status: NPKStatus::Depleted,
            k_status: NPKStatus::Depleted,
            plot_size_meters_squared:0.0,
        }
    }

    // SETTERS
    // in percent! Also sets soil condition!
    pub fn set_n_params(&mut self, val:f32, status:NPKStatus) {
        self.n_perc = val;
        self.n_status = status;
    }

    pub fn set_p_params(&mut self, val:f32, status:NPKStatus) {
        self.p_perc = val;
        self.p_status = status;
    }

    pub fn set_k_params(&mut self, val:f32, status:NPKStatus) {
        self.k_perc = val;
        self.k_status = status;
    }

    pub fn set_plot_size_meters_squared(&mut self, val:f32) {
        self.plot_size_meters_squared = val;
    }

    // CALCULATORS
    // This calculator will find out how much NPK values should be inputted
    // Feet area is for the scenario oz/100 feet squared for example! in this case put the 100 feet.
    pub fn calculate_n_fert_kg(&mut self, mut this_area_conv:AreaConversions, feet_area:f32) -> f32 {

        this_area_conv.set_feet(feet_area);

        // Actual calculations occur here.
        // These are at first in x oz/100ft^2
        // Nitrogen
        let n_oz_feet_sq:f32 = (-1.8*self.n_perc + 55.0)*self.n_status.return_mod();

        // Now we calculate all of the oz calcs.
        this_area_conv.set_oz(n_oz_feet_sq);
        let n_kg_meter_sq:f32 = this_area_conv.get_kg_per_meter_square_using_oz();

        // And now we calc how much to fill that area
        let kg_in_area:f32 = n_kg_meter_sq*self.plot_size_meters_squared;


        return kg_in_area;
    }

    pub fn calculate_p_fert_kg(&mut self, mut this_area_conv:AreaConversions, feet_area:f32) -> f32 {

        this_area_conv.set_feet(feet_area);

        // Actual calculations occur here.
        // These are at first in x oz/100ft^2
        // Phosphorouschaos head ost
        let p_oz_feet_sq:f32 = (-0.59045*self.p_perc + 36.764)*self.p_status.return_mod();

        // Now we calculate all of the oz calcs.
        this_area_conv.set_oz(p_oz_feet_sq);
        //println!("debug:{}",p_oz_feet_sq);
        let p_kg_meter_sq:f32 = this_area_conv.get_kg_per_meter_square_using_oz();

        //println!("debug:{}",p_kg_meter_sq);
        // And now we calc how much to fill that area
        let kg_in_area:f32 = p_kg_meter_sq*self.plot_size_meters_squared;


        return kg_in_area;
    }

    pub fn calculate_k_fert_kg(&mut self, mut this_area_conv:AreaConversions, feet_area:f32) -> f32 {

        this_area_conv.set_feet(feet_area);

        // Actual calculations occur here.
        // These are at first in x oz/100ft^2
        // Potassium
        let k_oz_feet_sq:f32 = (-1.6532*self.k_perc + 107.26)*self.k_status.return_mod();

        // Now we calculate all of the oz calcs.
        this_area_conv.set_oz(k_oz_feet_sq);
        let k_kg_meter_sq:f32 = this_area_conv.get_kg_per_meter_square_using_oz();

        // And now we calc how much to fill that area
        let kg_in_area:f32 = k_kg_meter_sq*self.plot_size_meters_squared;


        return kg_in_area;
    }
}

// This is concerned with ph calculations.
pub struct PHCalculator {
    ph_change:f32, // How much we expect the ph to change for this amount of material
    material_weight_pounds_per_feet:f32, // in POUNDS per FEET squared!

    cur_ph:f32, // What our current ph is.
    desired_ph:f32, // The ph that we desire.

    plot_size_meters_squared:f32, // total plot size


}

impl PHCalculator {
    pub fn new() -> PHCalculator {
        return PHCalculator {
            ph_change: 0.5,
            material_weight_pounds_per_feet: 2.5,
            plot_size_meters_squared: 10.0,
            cur_ph: 5.0,
            desired_ph: 6.0,
        };
    }

    // SETTERS
    pub fn set_ph_change(&mut self, val:f32) {
        self.ph_change = val;
    }

    pub fn set_cur_ph(&mut self, val:f32) {
        self.cur_ph = val;
    }

    pub fn set_desired_ph(&mut self, val:f32) {
        self.desired_ph = val;
    }

    pub fn set_mat_wei_pou_p_feet(&mut self, val:f32) {
        self.material_weight_pounds_per_feet = val;
    }

    pub fn set_plot_size_meters_squared(&mut self, val:f32) {
        self.plot_size_meters_squared = val;
    }

    // CALCULATORS
    // Feet area is for the scenario oz/100 feet squared for example or however many in that metric! in this case put the 100 feet.
    pub fn calculate_ph_fert_kg(&mut self, mut this_area_conv:AreaConversions, feet_area:f32) -> f32 {
        // First we convert the pounds per feet to kg per meter.
        this_area_conv.set_feet(feet_area);
        this_area_conv.set_pounds(self.material_weight_pounds_per_feet);
        let kg_per_meter:f32 = this_area_conv.get_kg_per_meter_square_using_pounds();

        //println!("DEBUG: {}", kg_per_meter);

        // Now we know how much kg per meter we need to change to a given ph.
        // What we do next is we get back how much material we need for a given plot size to change the ph.
        let kg_in_area:f32 = kg_per_meter*self.plot_size_meters_squared*((self.desired_ph - self.cur_ph)/self.ph_change);
        //println!("DEBUG: {}", kg_in_area);
        return kg_in_area;
    }


}
