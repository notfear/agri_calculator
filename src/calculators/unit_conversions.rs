// Units are defined here for easy conversion...
pub enum MassUnits {
    Oz,
    Pound,
}

impl MassUnits {
    pub fn to_kg(&mut self) -> f32 {
        match self {
            MassUnits::Oz => return 0.0283495,
            MassUnits::Pound => return 0.453592,
        }
    }
}

pub enum DistUnits {
    Feet,
}

impl DistUnits {
    pub fn to_meter(&mut self) -> f32 {
        match self {
            DistUnits::Feet => return 0.3048,
        }
    }
}
